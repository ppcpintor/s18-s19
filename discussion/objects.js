//[SECTION] Objects

	//Objects -> similar to an array is a collection of related data and/or functionalities. The purpose of an object is not only to store multipple data-sets but also to represent real world objects.

	//SYNTAX:
		//let/const variableName = {
			//key/property: value
		//}

	//NOTE: Information stored in objects are represented in key:value pairing.

//lets use this example to describe a real-world item or object.

let cellphone = {
	name: 'Nokia 3210',
	manufacturedDate: '1999',
	price: 5000
}; //'key' -> is also mostly referred to as a 'property' of an object.

console.log(cellphone);
console.log(typeof cellphone);

//How to store multiple objects
//You can use an array structure to store them.
let users = [
	{
		name: 'Anna',
		age: '23'
	},
	{
		name: 'Nicole',
		age: '18'
	},
	{
		name: 'Smith',
		age: '42'
	},
	{
		name: 'Pam',
		age: '13'
	},
	{
		name: 'Anderson',
		age: '26'
	}
]

console.log(users);
//now there is an alternative way when displaying values of an object collection.
console.table(users);

//Complex examples of JS objects
// NOTE: Different data types may be stored in an object's property creating more complex data structures

//When using JS Objects, keep in mind that you can also insert 'methods'.



let friend = {
	//properties
	firstName: 'Joe', //String or numbers
	lastName: 'Smite',
	isSingle: 'true', //boolean
	emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
	address: { //Objects
		city: 'Austin',
		state: 'Texas',
		country: 'USA'
	},
	//methods
	introduce: function () {
		console.log('Hello Meet My New Friend');
	},
	advice: function(){
		console.log('Give friend an advice to move on')
	},
	wingman: function() {
		console.log('Hype friend when new people')
	}
}
console.log(friend);

// [SECTION] ALTERNATE APPROACHES ON HOW TO DECLARE AN OBJECT IN JAVASCRIPT.

	//1. How to create an object using a constructor function?
	// constructor function -> we will need to create a reusable function in which we will declare a blueprint to describe the anatomy of an object that we wish to create.

	//to declare properties within a constructor function, you have to be familiar with the 'this' keyword

	function laptop(name, year, model) {
		this.name = name;
		this.manufacturedOn = year;
		this.model = model;
	};
	//'this' -> NOT A VARIABLE but a *keyword*.
	// this -> refers to the object in this example, 'this' refers to the 'global' object *laptop* because the this keyword is within the 

	//to create a new object that has the properties stated above, we will use the 'new' keyword for us to be able to create a new instance of an object.

let laptop1 = new laptop('Sony', 2008, 'Vaio');
let laptop2 = new laptop('Apple', 2019, 'Mac');
let laptop3 = new laptop('HP', 2015, 'HP');

	// What if I want to store them in a single container and display them in a tabular format?

let gadgets =[laptop1, laptop2, laptop3];
console.table(gadgets);

	//==> use and benefits: this is useful when creating 'instances' of several object that have the same data structures.

	//instance -> this refers to a concrete occurence of any object which emphasizes on the distinct/unique identity of the objects/subject.

	// example2:
	// Let's create an instance of multiple pokemon characters
	//use the 'this' keyword to associate each property with the global object and bind them with the respective values

function Pokemon(name, type, level, trainer) { //properties

	this.pokemonName = name;
	this.pokemonType = type;
	this.pokemonHealth = 2 * level;
	this.pokemonLevel = level;
	this.owner = trainer;
	//methods

	this.tackle = function(target) {
		console.log(this.pokemonName + ' tackled ' + target.pokemonName);
	};
	this.greetings = function() {
		console.log(this.pokemonName + ' says Hello!');
	};
}
let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash Ketchup');
let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak');

//display all pokemons in the console in a table format

let pokemons = [pikachu, ratata, snorlax];
console.table(pokemons);

//[SECTION] How to access Object Properties?
	//using '.' dot notation you can access properties of an object

	//SYNTAX: objectName.propertyName
	console.log(snorlax.owner);
	console.log(pikachu.pokemonType);

	//practice accessing a method of an object.
	pikachu.tackle(ratata);
	snorlax.greetings();

//[ALTERNATE APPROACH]
// SYNTAX: objectName['property name']
console.log(ratata['owner']);
console.log(ratata['pokemonLevel']); //IT WILL RETURN UNDEFINED, if you try to access a property that does NOT exists.

// Which is the best use case dot notation or square bracket?

// We will stick with dot notations when accessing properties of an object. as our convention when managing properties of an object.

// we will use square brackets when dealing with indexes of an array.

//[ADDITIONAL KNOWLEDGE]:
	//Kepp this in mind, you can write and declare objects this way:

	//to create an object, you will use object literals '{}'

	let trainer = {; //empty object
	console.log(trainer);

	//will I be able to add properties from outside an object? YES

	trainer.name = 'Ash Ketchup';
	console.log(trainer);
	trainer.friends = ['Misty', 'Brock', 'Tracey'];
	console.log(trainer); //array

	//method
	trainer.speak = function()
	console.log('Pikachu, I choose you!');
}
trainer.speak();
//possible approach